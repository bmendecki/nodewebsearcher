var request = require('request');

exports.start = function(req,res){
    res.render('start.ejs');
};

exports.result = function(req,res){
        
        var userInput = req.body.lista;
        var phrase = req.body.phrase;
        var listOfUrls = userInput.split('\n');                    
        
        var geturslResponsePromise = function(elem) {
            return new Promise(function(resolve, reject){
            elem = ( elem.indexOf('http') !== -1 ) ? elem : 'http://' + elem;
            var options = {
                headers: {'user-agent': 'Mozilla','referer': 'http://localhost:3000/' },
                url : elem
            };
            request(options, function(error, response, html) {

                try {
                    if (html.indexOf(phrase) > 0) {
                        resolve('>>>>>> znaleziono ' + elem );
                        }
                    else {
                        resolve(elem + ' : nie znaleziono');
                    }
                    
                }
                catch(err) {
                    reject('Error: '+ err.message);
                }
        
            });// request
           });// promise
        };
       
        
        var responsePromises = listOfUrls.map(geturslResponsePromise);
       
        Promise.all(responsePromises).then(function(results){                     
           
            res.render('result.ejs', { 
                              results : results,                 
                              phrase : phrase
            });    
               
        }) 
};

exports.not_found = function(req, res, next){
    res.redirect('/scrap');
}
