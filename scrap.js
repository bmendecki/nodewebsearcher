var express = require('express');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var fs = require("fs");
var app     = express();

var routes = require('./routes');

app.get('/scrap/', routes.start);

app.post('/scrap/result',urlencodedParser, routes.result); 
   
  
app.use(function(req, res, next){
    res.redirect('/scrap');
});  

console.log("listenig on 3000")
app.listen(3000);