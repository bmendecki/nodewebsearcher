### NodeWebSearcher ###

Scrap and search phrases on websites with NodeJs.

Requirements:

- NodeJs installed
- free port 3000 (or other)

Usage:

- go to http://localhost:3000/scrap
- put list of urls and phrase
- enjoy :)